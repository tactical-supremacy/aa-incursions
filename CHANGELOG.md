# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [] - yyyy-mm-dd

### Added

### Changed

### Fixed

## [0.7.0a] - 2024-02-17

Maintenance Release-ish

### Fixes

- System names are comma spaced
- Add two more constellations that have never had incursion data
- queueonce the update task
- Attempt to again fix the uptime and utilization data in the discord embed
- support py13
- use standard format useragent

## [0.6.0a] - 2024-07-05

### Added

- EVERef import code
- Basic webui
- Waitlist model backend, still very WIP

### Changed

- Moved High/Low/Null notify setting into the webhooks themselves, now you can have a highsec only webhook and a Lowsec only webhook

### Fixed

- A lot of small exceptions, webhook formatting issues
- Added minimum 3 day spawn to mom for HS and 1 day for LS

## [0.5.0a] - 2024-05-21

### Changed

- More details on update hook, be clearer about region/secstatus layout
- Expanded admin details
- Influence optimizations

### Fixed

- Detect Withdrawing state properly
- Add exceptions if a Focus isnt set
- Handle repulling of the same influence data better
- Handle partially complete incursions better, primarily for new installs.

## [0.4.3a] - 2024-05-21

### Fixed

- Removed a translation django dislikes

## [0.4.2a] - 2024-05-21

Maintenance release. Gitlab CI was unhappy about python3.12 so v0.3 and v0.4 had not pushed to PyPi.

## [0.4.0a] - 2023-11-08

### Added

- Translation Support
- Historical import support from EVERef, both commands to replicate the data yourself and a simple fixture for reasonably up to date data.
- Trig and Edencom routing checks
- More accurate routing using AA-Routing
- Auto move focus to HS incursion (setting)

### Changed

- Updated the static incursion data from historical reference (EVE Ref)
- Updated the static incursion data from Dotlan incursions
- Added AA Routing as a dependency
- removed local swagger spec :ccp:

### Fixed

- tests

## [0.3.0a] - 2023-10-08

Maintenance release

### Added

- Boss Spawned notifications #1

### Changed

- Add dotlan URL to region and constellation, to do systems later once they have had other work #1

### Fixed

## [0.2.0a] - 2023-08-07

### Added

- More commands to the cog, set focus, get incursions, incursion details etc.

### Fixed

- Fixed timestamps used in withdrawing/established/ended webhooks.

## [0.1.0a] - 2023-08-03

Initial release, containing mostly django framework and the Discord Bot cogs.
