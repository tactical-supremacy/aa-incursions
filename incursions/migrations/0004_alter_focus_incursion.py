# Generated by Django 4.0.10 on 2023-08-04 04:32

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('incursions', '0003_focus'),
    ]

    operations = [
        migrations.AlterField(
            model_name='focus',
            name='incursion',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='incursions.incursion', verbose_name='Current Focus'),
        ),
    ]
