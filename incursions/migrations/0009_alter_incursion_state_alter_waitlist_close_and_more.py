# Generated by Django 4.2 on 2024-08-24 06:03

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('authentication', '0023_alter_userprofile_language'),
        ('incursions', '0008_general_remove_incursionsconfig_security_high_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='incursion',
            name='state',
            field=models.CharField(choices=[('established', 'Incursion established'), ('mobilizing', 'Incursion mobilizing'), ('withdrawing', 'Incursion withdrawing'), ('ended', 'Ended')], default='established', max_length=50, verbose_name='The state of this incursion'),
        ),
        migrations.AlterField(
            model_name='waitlist',
            name='close',
            field=models.DateTimeField(verbose_name='Close Time'),
        ),
        migrations.AlterField(
            model_name='waitlist',
            name='fc',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='authentication.user', verbose_name='Fleet Commander'),
        ),
        migrations.AlterField(
            model_name='waitlist',
            name='open',
            field=models.DateTimeField(verbose_name='Open Time'),
        ),
    ]
