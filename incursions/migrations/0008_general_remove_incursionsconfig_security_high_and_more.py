# Generated by Django 4.2.13 on 2024-07-05 05:24

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('eveonline', '0017_alliance_and_corp_names_are_not_unique'),
        ('authentication', '0023_alter_userprofile_language'),
        ('eveuniverse', '0010_alter_eveindustryactivityduration_eve_type_and_more'),
        ('incursions', '0007_alter_focus_options'),
    ]

    operations = [
        migrations.CreateModel(
            name='General',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
            options={
                'permissions': (('basic_incursions', 'Can view incursion data'),),
                'managed': False,
                'default_permissions': (),
            },
        ),
        migrations.RemoveField(
            model_name='incursionsconfig',
            name='security_high',
        ),
        migrations.RemoveField(
            model_name='incursionsconfig',
            name='security_low',
        ),
        migrations.RemoveField(
            model_name='incursionsconfig',
            name='security_null',
        ),
        migrations.AddField(
            model_name='webhook',
            name='security_high',
            field=models.BooleanField(default=True, verbose_name='Notify on High Security Incursions'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='webhook',
            name='security_low',
            field=models.BooleanField(default=True, verbose_name='Notify on Low Security Incursions'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='webhook',
            name='security_null',
            field=models.BooleanField(default=True, verbose_name='Notify on Null Security Incursions'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='incursion',
            name='faction',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='eveonline.evefactioninfo', verbose_name='The attacking Faction'),
        ),
        migrations.CreateModel(
            name='WaitlistShip',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('weight', models.IntegerField(verbose_name='Weight')),
                ('ship', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='eveuniverse.evetype', verbose_name='Ship Hull')),
            ],
            options={
                'verbose_name': 'Waitlist Ship Weight',
                'verbose_name_plural': 'Waitlist Ship Weightings',
            },
        ),
        migrations.CreateModel(
            name='WaitlistEntry',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('time', models.DateTimeField(auto_now_add=True, verbose_name='Waitlist Join Time')),
                ('note', models.TextField(verbose_name='Any extended note, Box #, begging to let ishtar-trash into fleet')),
                ('role', models.CharField(choices=[('dps', 'Mainline DPS'), ('vindicator', 'Vindicator'), ('logistics', 'Logistics'), ('boosts', 'Fleet Boosts / Other Utility')], default='dps', max_length=50, verbose_name='Ship role, usually applied to split the waitlist.')),
                ('ship', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='incursions.waitlistship', verbose_name='Ship Hull, with applied Weight')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='authentication.user', verbose_name='User')),
            ],
            options={
                'verbose_name': 'Waitlist Entry',
                'verbose_name_plural': 'Waitlist Entries',
            },
        ),
        migrations.CreateModel(
            name='Waitlist',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('open', models.DateTimeField(verbose_name='')),
                ('close', models.DateTimeField(verbose_name='')),
                ('fc', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='authentication.user', verbose_name='')),
            ],
            options={
                'verbose_name': 'Waitlist',
                'verbose_name_plural': 'Waitlist',
            },
        ),
    ]
